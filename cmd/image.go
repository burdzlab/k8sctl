package cmd

import (	
	"log"
	// "time"
	"fmt"
	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"	
	"bitbucket.org/burdzlab/k8sctl/internal/kube/connect"	
	"bitbucket.org/burdzlab/k8sctl/internal/tools"		
	"bitbucket.org/burdzlab/k8sctl/internal/kube/deployments"	
	"k8s.io/apimachinery/pkg/api/errors"
	// "k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/util/retry"
	appsv1 "k8s.io/api/apps/v1"
)

var imageCmd = &cobra.Command{
	Use:   "image",
	Short: "Change Deployment Image",
	Long: ``,	
	Run: func(cmd *cobra.Command, args []string) {

		image, _ := cmd.Flags().GetString("image")
		containerName, _ := cmd.Flags().GetString("container")
		deployName, _ := cmd.Flags().GetString("deployment")
		namespace, _ := cmd.Flags().GetString("namespace")
		filename, _ := cmd.Flags().GetString("filename")

		// watch, _ := cmd.Flags().GetBool("watch")		
		dclient := connect.ClientSet.AppsV1().Deployments(namespace)
		deployment, err := dclient.Get(deployName, metav1.GetOptions{})		
		
		if errors.IsNotFound(err) {
			if filename == "" {
				log.Fatalf("There is no Deployment: %s and there is no file do apply", deployName)
			}
			log.Println("There is no Deployment starting create Deployment", containerName)
			
			objects := tools.ReadYaml(filename)

			for _, obj := range objects {
				switch o := obj.(type) {
					case *appsv1.Deployment:
						position := deployments.GetContainerPosition(containerName, o)						
						o.SetName(deployName)
						o.Spec.Template.Spec.Containers[position].Image = image
						o.SetAnnotations(map[string]string{"kubernetes.io/change-cause": fmt.Sprint("\"k8sctl image:\" Creating Deploy - image -> ", image) })		
						log.Printf("Create Deploy %s - image -> %s", o.GetName(), image)						
						deployments.CreateOrUpdate(namespace, o, deployName)								
				}
			}

		// Changing deployment image
		} else if err == nil {

			position := deployments.GetContainerPosition(containerName, deployment)

			if deployment.Spec.Template.Spec.Containers[position].Image == image {
				// deployment.Annotations["DateTryRestart"] = time.Now().String()
				log.Printf("Image Deploy does not changed -> %s,  deployment: %s", image, deployment.GetName())
			} else {				
				log.Printf("Deploy %s - Old image -> %s", deployment.GetName(), deployment.Spec.Template.Spec.Containers[position].Image)
				log.Printf("Deploy %s - New image -> %s", deployment.GetName(), image)
				deployment.Annotations["kubernetes.io/change-cause"] = fmt.Sprint("\"k8sctl image:\" Update Image -> ", image)
				deployment.Spec.Template.Spec.Containers[position].Image = image
			}

			err = retry.RetryOnConflict(retry.DefaultRetry, func() error {						
				_ , err :=  dclient.Update(deployment)			
				return err
			});
			if err != nil {
				panic(err);
			}	
			log.Printf("Update commad to deploy %s applied - image: %s", deployment.GetName(), image)

		} else {
			panic(err.Error())
		}
	},
}

func init() {
	rootCmd.AddCommand(imageCmd)	
	
	imageCmd.PersistentFlags().StringP("image", "i", "", "Name of Image")
	imageCmd.MarkPersistentFlagRequired("image")

	imageCmd.Flags().StringP("container", "c", "", "Container Name")

	// imageCmd.Flags().BoolP("watch", "w", false, "Watch image change and report")

	imageCmd.PersistentFlags().StringP("namespace",  "n", "", "Namespace name")
	imageCmd.MarkPersistentFlagRequired("namespace")

	imageCmd.PersistentFlags().StringP("deployment",  "d", "", "Deployment Name")
	imageCmd.MarkPersistentFlagRequired("deployment")

	imageCmd.PersistentFlags().StringP("filename",  "f", "", "That contains the configuration to apply")
}
