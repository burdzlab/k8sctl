package cmd

import (	
	"log"
	"time"	
	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"	
	"bitbucket.org/burdzlab/k8sctl/internal/kube/connect"	
	"k8s.io/client-go/util/retry"
)

var restartCmd = &cobra.Command{
	Use:   "restart",
	Short: "Restart Deployment",
	Long: ``,	
	Run: func(cmd *cobra.Command, args []string) {
		deployName, _ := cmd.Flags().GetString("deployment")
		namespace, _ := cmd.Flags().GetString("namespace")
			
		dclient := connect.ClientSet.AppsV1().Deployments(namespace)
		deployment, err := dclient.Get(deployName, metav1.GetOptions{})	
		
		if err != nil {
			panic(err);
		}		
		
		annotations := deployment.Spec.Template.GetAnnotations()
		if annotations == nil {
			annotations = make(map[string]string)
		}
		annotations["DateRestart"] = time.Now().String()
		deployment.Spec.Template.SetAnnotations(annotations)

		err = retry.RetryOnConflict(retry.DefaultRetry, func() error {						
			_ , err :=  dclient.Update(deployment)			
			return err
		});
		if err != nil {
			panic(err);
		}	
		log.Printf("Restart Deploy %s Namespace %s", deployment.GetName(), namespace)
	},
}

func init() {
	rootCmd.AddCommand(restartCmd)	
	
	restartCmd.PersistentFlags().StringP("namespace",  "n", "", "Namespace name")
	restartCmd.MarkPersistentFlagRequired("namespace")

	restartCmd.PersistentFlags().StringP("deployment",  "d", "", "Deployment Name")
	restartCmd.MarkPersistentFlagRequired("deployment")
}
