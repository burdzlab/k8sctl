package cmd

import (	
	"log"
	"strconv"
	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"	
	"bitbucket.org/burdzlab/k8sctl/internal/kube/connect"
	"k8s.io/apimachinery/pkg/api/errors"
	v1beta "k8s.io/api/extensions/v1beta1"
	"k8s.io/client-go/util/retry"
)

var undoCmd = &cobra.Command{
	Use:   "undo",
	Short: "rollback deployment last version",
	Long: ``,	
	Run: func(cmd *cobra.Command, args []string) {
		
		revision, _ := cmd.Flags().GetInt64("revision")
		deployName, _ := cmd.Flags().GetString("deployment")
		namespace, _ := cmd.Flags().GetString("namespace")

		dclient := connect.ClientSet.AppsV1().Deployments(namespace)
		deployment, err := dclient.Get(deployName, metav1.GetOptions{})
	
				
		if errors.IsNotFound(err) {
			log.Fatalf("There is no Deployment: %s ", deployName)

		// Undo to last version deployment
		} else if err == nil {
			
			if revision == 0 {				
				currentRevisionS, exist := deployment.Annotations["deployment.kubernetes.io/revision"]
				if !exist {
					panic("dedeployment.kubernetes.io/revision  not exist");
				}
				currentRevisionI , err :=  strconv.ParseInt(currentRevisionS, 10, 64)
				if err != nil {
					panic(exist);
				}
				revision = currentRevisionI - 1
			}
			
			log.Println("REVISION ", revision)
			
			dr := new(v1beta.DeploymentRollback)
			dr.Name = deployment.Name
			dr.UpdatedAnnotations = deployment.GetAnnotations()
			dr.RollbackTo = v1beta.RollbackConfig{Revision: revision}

			dclientEx := connect.ClientSet.ExtensionsV1beta1().Deployments(namespace)

			// Rollback
			err = retry.RetryOnConflict(retry.DefaultRetry, func() error {						
				err := dclientEx.Rollback(dr)		
				return err
			});
			if err != nil {
				panic(err);
			}
		} else {
			panic(err.Error())
		}
	},
}

func init() {
	rootCmd.AddCommand(undoCmd)	
	
	undoCmd.PersistentFlags().StringP("namespace",  "n", "", "Namespace name")
	undoCmd.MarkPersistentFlagRequired("namespace")

	undoCmd.PersistentFlags().StringP("deployment",  "d", "", "Deployment Name")
	undoCmd.MarkPersistentFlagRequired("deployment")

	undoCmd.Flags().Int64("revision", 0, "Revision Number")
}