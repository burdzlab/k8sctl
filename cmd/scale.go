package cmd

import (
	"log"
	"os"
	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"	
	"bitbucket.org/burdzlab/k8sctl/internal/kube/connect"
	"k8s.io/client-go/util/retry"
)

// scaleCmd represents the scale command
var scaleCmd = &cobra.Command{
	Use:   "scale",
	Short: "Change Deployment Scale",
	Long: ``,	
	Run: func(cmd *cobra.Command, args []string) {

		replicas, _ := cmd.Flags().GetInt32("replicas")
		deployName, _ := cmd.Flags().GetString("deployment")
		namespace, _ := cmd.Flags().GetString("namespace")
		
		dclient := connect.ClientSet.AppsV1().Deployments(namespace)
		deployment, err := dclient.Get(deployName, metav1.GetOptions{})
		
		if err != nil {
			log.Fatalln("failed to get deployment:", err)
		}

		if replicas ==  *deployment.Spec.Replicas {
			log.Printf("Deploy %s replicas %d", deployName, replicas)
			os.Exit(0)
		}		
		
		// Set changing Replicas		
		err = retry.RetryOnConflict(retry.DefaultRetry, func() error {			
			*deployment.Spec.Replicas = replicas	
			_ , err :=  dclient.Update(deployment)			
			return err
		});
		if err != nil {
			panic(err)
		}

		log.Printf("Deploy %s replicas %d applied", deployment.GetName(), *deployment.Spec.Replicas)
	},
}

func init() {
	rootCmd.AddCommand(scaleCmd)

	scaleCmd.PersistentFlags().StringP("namespace",  "n", "", "Namespace name")
	scaleCmd.MarkPersistentFlagRequired("namespace")

	scaleCmd.PersistentFlags().StringP("deployment",  "d", "", "Deployment Name")
	scaleCmd.MarkPersistentFlagRequired("deployment")
	
	scaleCmd.Flags().Int32("replicas", 0, "Numbers of Replicas")
	scaleCmd.MarkPersistentFlagRequired("replicas")
}