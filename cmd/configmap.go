package cmd

import (
	"log"	
	"github.com/spf13/cobra"	
	"bitbucket.org/burdzlab/k8sctl/internal/kube/configmaps"
	"bitbucket.org/burdzlab/k8sctl/internal/tools"
	corev1 "k8s.io/api/core/v1"
)

// configCmd represents the scale command
var configCmd = &cobra.Command{
	Use:   "configmap",
	Short: "Config Yaml File",
	Long: ``,
	Run: func(cmd *cobra.Command, args []string) {		

		filename, _ := cmd.Flags().GetString("filename")
		namespace, _ := cmd.Flags().GetString("namespace")
		name, _ := cmd.Flags().GetString("name")

		objects := tools.ReadYaml(filename)
		for _, obj := range objects {

			switch o := obj.(type) {				
				case *corev1.ConfigMap:
					if name != "" {
						o.SetName(name)
					}
					log.Printf("Create ConfigMap %s", o.GetName())
					configmaps.CreateOrUpdate(namespace, o)
				default:
					log.Println("Only ConfigMap is allow to be created.")									
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(configCmd)

	configCmd.PersistentFlags().StringP("namespace",  "n", "", "Namespace name")
	configCmd.MarkPersistentFlagRequired("namespace")

	configCmd.PersistentFlags().StringP("filename",  "f", "", "that contains the configuration to apply")
	configCmd.MarkPersistentFlagRequired("filename")

	configCmd.Flags().String("name", "", "Configmap Name")
	configCmd.MarkPersistentFlagRequired("name")
}