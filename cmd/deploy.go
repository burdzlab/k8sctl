package cmd

import (
	"fmt"
	"log"	
	"time"
	"strings"
	"github.com/spf13/cobra"	
	"bitbucket.org/burdzlab/k8sctl/internal/kube/deployments"	
	"bitbucket.org/burdzlab/k8sctl/internal/kube/services"
	"bitbucket.org/burdzlab/k8sctl/internal/kube/configmaps"
	"bitbucket.org/burdzlab/k8sctl/internal/tools"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"	
)

// deployCmd represents the scale command
var deployCmd = &cobra.Command{
	Use:   "deploy",
	Short: "Deploy Yaml File",
	Long: ``,	
	Run: func(cmd *cobra.Command, args []string) {		

		filename, _ := cmd.Flags().GetString("filename")
		namespace, _ := cmd.Flags().GetString("namespace")

		deployName, _ := cmd.Flags().GetString("deployment")

		image, _ := cmd.Flags().GetString("image")
		containerName, _ := cmd.Flags().GetString("container")

		cmap, _ := cmd.Flags().GetString("map")
		mapname, _ := cmd.Flags().GetString("mapname")

		if cmap != "" {
			objects := tools.ReadYaml(cmap)
			for _, obj := range objects {
	
				switch o := obj.(type) {
					case *corev1.ConfigMap:
						if mapname != "" {
							o.SetName(mapname)
						}
						log.Printf("Create ConfigMap %s", o.GetName())
						configmaps.CreateOrUpdate(namespace, o)
					default:
						log.Println("Only ConfigMap is allow to be created.")									
				}
			}
		}

		objects := tools.ReadYaml(filename)
		for _, obj := range objects {

			switch o := obj.(type) {
				case *appsv1.Deployment:
					var position int
					if image != "" {
						position = deployments.GetContainerPosition(containerName, o)
						o.Spec.Template.Spec.Containers[position].Image = image
					}
					
					o.SetAnnotations(map[string]string{"kubernetes.io/change-cause": fmt.Sprint("k8sctl deploy: image -> ", o.Spec.Template.Spec.Containers[position].Image) })							
					o.SetName(deployName)

					annotations := o.Spec.Template.GetAnnotations()
					if annotations == nil {
						annotations = make(map[string]string)
					}
					annotations["DateRestart"] = time.Now().String()
					o.Spec.Template.SetAnnotations(annotations)
					deployments.CreateOrUpdate(namespace, o, deployName)

				case *corev1.Service:
					o.SetName(strings.ReplaceAll(deployName, ".", "-"))
					log.Printf("Create Service %s", o.GetName())
					services.CreateOrUpdate(namespace, o, deployName)

				default:
					log.Println("Only Service and Deployment are allow to be created.")
			}
		}	
	},
}

func init() {
	rootCmd.AddCommand(deployCmd)

	deployCmd.Flags().StringP("container", "c", "", "Container Name")
	deployCmd.Flags().StringP("image", "i", "", "Name of Image")

	deployCmd.PersistentFlags().StringP("namespace",  "n", "", "Namespace name")
	deployCmd.MarkPersistentFlagRequired("namespace")

	deployCmd.PersistentFlags().StringP("filename",  "f", "", "that contains the configuration to apply")
	deployCmd.MarkPersistentFlagRequired("filename")

	deployCmd.Flags().StringP("map", "m", "", "That contains the configmaps to apply")
	deployCmd.Flags().String("mapname", "", "Configmap Name")

	deployCmd.PersistentFlags().StringP("deployment",  "d", "", "Deployment Name")
	deployCmd.MarkPersistentFlagRequired("deployment")
}