FROM golang:1.12.6 as builder

WORKDIR /go/src/bitbucket.org/burdzlab/k8sctl
COPY . /go/src/bitbucket.org/burdzlab/k8sctl

RUN  mkdir -p /usr/local/share/ca-certificates/proxy
COPY build/proxy.crt /usr/local/share/ca-certificates/proxy
RUN chmod 644 /usr/local/share/ca-certificates/proxy/proxy.crt
RUN update-ca-certificates

#RUN apt-get install go-dep
RUN curl -k https://raw.githubusercontent.com/golang/dep/master/install.sh | bash

RUN dep ensure
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o k8sctl .

FROM alpine:latest
#RUN apk --no-cache add ca-certificates

WORKDIR /root/
COPY --from=builder /go/src/bitbucket.org/burdzlab/k8sctl/k8sctl /bin

CMD ["/bin/sh"]