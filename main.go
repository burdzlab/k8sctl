package main

import (
  "log"
  "bitbucket.org/burdzlab/k8sctl/cmd"
)

func main() {
  log.SetPrefix("k8sctl: ")
  cmd.Execute()
}
