package connect

import (	
	"log"
	"os"
	"path/filepath"
	
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"	
)

var ClientSet *kubernetes.Clientset

func init() {

	log.Println("Kubernetes config start Load")

	kubeconfig := os.Getenv("KUBECONFIG")
	if kubeconfig == "" {
		kubeconfig = filepath.Join(
			os.Getenv("HOME"), ".kube", "config",
		)
	}
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		log.Fatal(err)
	}
	ClientSet = kubernetes.NewForConfigOrDie(config)	
}