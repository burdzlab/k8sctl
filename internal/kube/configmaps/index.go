package configmaps

import (
	"log"
	corev1 "k8s.io/api/core/v1"	
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"bitbucket.org/burdzlab/k8sctl/internal/kube/connect"	
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/client-go/util/retry"
)
// CreateOrUpdate if not exist
func CreateOrUpdate(namespace string, obj *corev1.ConfigMap) {

	// Change Namespace
	if namespace != "" {
		obj.SetNamespace(namespace)
	}

	mclient :=  connect.ClientSet.CoreV1().ConfigMaps(namespace)	

	// Check ConfigMap already exist
	cmap , err := mclient.Get(obj.GetName(), metav1.GetOptions{})
	
	if err == nil {
		// Update ConfigMap
		obj.ResourceVersion = cmap.ResourceVersion

		err = retry.RetryOnConflict(retry.DefaultRetry, func() error {	
			log.Println("ConfigMap Update: ", obj.GetName())			
			_ , err :=  mclient.Update(obj)			
			return err
		});

		if err != nil {
			panic(err);
		}

	} else if errors.IsNotFound(err) {
		// Create a new one
		err = retry.RetryOnConflict(retry.DefaultRetry, func() error {
			log.Println("ConfigMap Create: ", obj.GetName())				
			_ , err :=  mclient.Create(obj)			
			return err
		});
		if err != nil {
			panic(err);
		}
	}
}