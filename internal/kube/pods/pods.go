
package pods

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"bitbucket.org/burdzlab/k8sctl/internal/kube/connect"	
)

func StatusReady(namespace string, options metav1.ListOptions) (bool, error) {
	pods, err := connect.ClientSet.CoreV1().Pods(namespace).List(options)
	if err != nil {
		return false, err
	}

	for _, item := range pods.Items {
		for _, status := range item.Status.ContainerStatuses {			
			if !status.Ready {
				return false, nil
			}
		}
	}
	return true, nil
}