package images


import (
	"fmt"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"	
	"bitbucket.org/burdzlab/k8sctl/internal/kube/connect"
	"bitbucket.org/burdzlab/k8sctl/internal/tools"
)

// ListImages returns a list of container images running in the provided namespace
func ListAllImages(namespace string) ([]string, error) {
	core := connect.ClientSet.CoreV1()
	pl, err := core.Pods(namespace).List(metav1.ListOptions{})
	if err != nil {
		return nil, fmt.Errorf("%s %s",err, "getting pods")
	}

	var images []string
	for _, p := range pl.Items {
		for _, c := range p.Spec.Containers {
			images = append(images, c.Image)
		}
	}
	return tools.RemoveDuplicates(images), nil
}