package services

import (
	"log"
	corev1 "k8s.io/api/core/v1"	
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"bitbucket.org/burdzlab/k8sctl/internal/kube/connect"	
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/client-go/util/retry"
)
// CreateOrUpdate Service if not exist
func CreateOrUpdate(namespace string, serviceObj *corev1.Service, deployName string) {

	// Change Namespace
	if namespace != "" {
		serviceObj.Namespace = namespace
	}

	// Change APP Labels
	labels := serviceObj.GetLabels()
	labels["app"] = deployName
	serviceObj.SetLabels(labels)
	serviceObj.Spec.Selector["app"] = deployName

	sclient :=  connect.ClientSet.CoreV1().Services(namespace)
	// Check Service already exist
	ser , err := sclient.Get(serviceObj.GetName(), metav1.GetOptions{})
	
	if err == nil {
		// Update Service
		serviceObj.ResourceVersion = ser.ResourceVersion
		serviceObj.Spec.ClusterIP = ser.Spec.ClusterIP

		err = retry.RetryOnConflict(retry.DefaultRetry, func() error {	
			log.Println("Service Update: ", serviceObj.GetName())			
			_ , err :=  sclient.Update(serviceObj)			
			return err
		});

		if err != nil {
			panic(err);
		}

	} else if errors.IsNotFound(err) {
		// Create a new one
		err = retry.RetryOnConflict(retry.DefaultRetry, func() error {
			log.Println("Service Create: ", serviceObj.GetName())				
			_ , err :=  sclient.Create(serviceObj)			
			return err
		});
		if err != nil {
			panic(err);
		}
	}
}