package deployments

import (
	"log"	
	appsv1 "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"bitbucket.org/burdzlab/k8sctl/internal/kube/connect"	
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/client-go/util/retry"
)
// CreateOrUpdate Deployment if not exist
func CreateOrUpdate(namespace string, deployObj *appsv1.Deployment, deployName string) {

	// Change Namespace
	if namespace != "" {
		deployObj.Namespace = namespace
	}

	// Change APP Labels
	deployObj.Spec.Selector.MatchLabels["app"] = deployName

	labels := deployObj.Spec.Template.GetLabels()
	labels["app"] = deployName
	deployObj.Spec.Template.SetLabels(labels)

	dclient := connect.ClientSet.AppsV1().Deployments(namespace)

	// Check Deploy already exist
	currentDeploy, err := dclient.Get(deployObj.GetName(), metav1.GetOptions{})
	
	if err == nil {
		deployObj.ResourceVersion = currentDeploy.ResourceVersion
		deployObj.Spec.Replicas = currentDeploy.Spec.Replicas
		err = retry.RetryOnConflict(retry.DefaultRetry, func() error {						
			log.Printf("Updating Deploy %s", deployObj.GetName())	
			_ , err :=  dclient.Update(deployObj)
			return err
		});

		if err != nil {
			panic(err);
		}
		log.Printf("Deploy Updated %s", deployObj.GetName())

	} else if errors.IsNotFound(err) {
		fmt.Print("PASSOU")
		os.Exit(3)	
		err = retry.RetryOnConflict(retry.DefaultRetry, func() error {	
			log.Printf("Creating Deploy %s", deployObj.GetName())			
			_ , err :=  dclient.Create(deployObj)			
			return err
		});
		if err != nil {
			panic(err);
		}
		log.Printf("Deploy Crated %s", deployObj.GetName())
	}	
}