package deployments

import (
	"log"
	appsv1 "k8s.io/api/apps/v1"	
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"	
	"bitbucket.org/burdzlab/k8sctl/internal/kube/connect"
	"k8s.io/apimachinery/pkg/labels"
)

// GetReplicasSet
func GetReplicasSet(deployObj *appsv1.Deployment) *appsv1.ReplicaSetList {

	labelSelector := metav1.LabelSelector{MatchLabels: deployObj.Labels}
	listOptions := metav1.ListOptions{
		LabelSelector: labels.Set(labelSelector.MatchLabels).String(),
	}

	replicaSets := connect.ClientSet.AppsV1().ReplicaSets(deployObj.GetNamespace())
	replicas, err := replicaSets.List(listOptions)

	if err != nil {
		log.Fatalln("failed to get replicas:", err)
	}
	return replicas
}