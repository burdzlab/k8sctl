package deployments

import (
	"log"
	appsv1 "k8s.io/api/apps/v1"	
)
// GetContainerPosition get the position of container in the deployment
func GetContainerPosition(containerName string, deployObj *appsv1.Deployment) int {

	/// set image in the array position
	var position int		

	if containerName != "" {
		var hasContainer bool =  false

		for i, c := range deployObj.Spec.Template.Spec.Containers {				
			if c.Name == containerName {
				position = i
				hasContainer = true					
				break
			}
		}
		if !hasContainer {
			log.Fatalf("Container name %s does not exist in deploy %s!", containerName, deployObj.GetName())
		}
	}
	return position
}