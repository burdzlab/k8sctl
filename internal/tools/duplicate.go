package tools

func RemoveDuplicates(s []string) []string {
      m := make(map[string]bool)
      for _, item := range s {
              if _, ok := m[item]; !ok {
				m[item] = true 
              }
      }
      var result []string
      for item, _ := range m {
		result = append(result, item)
      }
      return result
}