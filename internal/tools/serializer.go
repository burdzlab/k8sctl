package tools


import (
	"log"
	"io/ioutil"
	"strings"
	"k8s.io/client-go/kubernetes/scheme"
	// appsv1 "k8s.io/api/apps/v1"
	// corev1 "k8s.io/api/core/v1"	
)

func LoadYaml(filename string)  []interface{} {
	dataByte, err := ioutil.ReadFile(filename)
	if err!=nil {
		log.Fatal(err)
	}

	dataString := string(dataByte)
	yamlfiles := strings.Split(dataString, "---")

	var iarray []interface{}
	
	for _, file := range yamlfiles {
		file = strings.Trim(file, " ")
		file = strings.Trim(file, "\n")
		if file == "\n" || file == "" {
			// ignore empty cases
			continue
		}

		decode := scheme.Codecs.UniversalDeserializer().Decode
		obj, _, err := decode([]byte(file), nil, nil)
					
		if err != nil {
			panic(err.Error())		
		}

		iarray = append(iarray, obj)	
		// log.Println(obj)
		// log.Println(iarray)

		/*

		//log.Printf("%++v\n\n", obj.GetObjectKind())
		//log.Printf("%++v\n\n", obj)

		switch o := obj.(type) {
			case *appsv1.Deployment:
				// deployment.Create(namespace, o)
			case *corev1.Service:
				log.Println("Service")
			default:
				log.Println(o)
				log.Fatal("Only Service and Deployment are allow to be created.")									
		}
		*/
		/*	
		if err != nil {
			log.Println(fmt.Sprintf("Error while decoding YAML object. Err was: %s", err))
			continue
		}

		if !acceptedK8sTypes.MatchString(groupVersionKind.Kind) {
			log.Printf("The custom-roles configMap contained K8s object types which are not supported! Skipping object with type: %s", groupVersionKind.Kind)
		} else {
			retVal = append(retVal, obj)
		}
		*/	
	}

	return iarray
}