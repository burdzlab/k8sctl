package tools


import (
	"log"
	"io/ioutil"
	"strings"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/apimachinery/pkg/runtime"
)
func ReadYaml(filename string) []runtime.Object {
	dataByte, err := ioutil.ReadFile(filename)
	if err!=nil {
		log.Fatal(err)
	}

	dataString := string(dataByte)
	yamlfiles := strings.Split(dataString, "---")
	var objects []runtime.Object
	
	for _, file := range yamlfiles {
		file = strings.Trim(file, " ")
		file = strings.Trim(file, "\n")
		if file == "\n" || file == "" {
			// ignore empty cases
			continue
		}		

		decode := scheme.Codecs.UniversalDeserializer().Decode
		obj, _, err := decode([]byte(file), nil, nil)
					
		if err != nil {
			log.Fatal(err)			
			continue
		}
		objects = append(objects, obj)		
	}
	return objects
}