## DEPLOY Kubernetes Cluster Service

docker build -t andersondborges/k8sctl:1.0 -f build/Dockerfile .


### COMMANDS

#### deploy
  - -c, --container string   Container Name
  - -f, --filename string    Contains the configuration to apply 
  - -i, --image string       Name of Image
  - -n, --namespace string   Namespace name
  - -m, --map                Configmap file
  - --mapname                Configmap change name
  - -d   --deployment        Deploy Name


Deve-se passar como obrigatório "namespace" e "filename"

Caso passe nome da image, o será substituído pelo primeiro container encotrado no POD.

Para alterar imagem de um container específico deve-se passar o nome do container em "container"

Em caso do deployment ou Service não existir será criar um novo e o mesmo existindo será atualizado.

Sendo um Service parametro container e imagem serão ignorados.

arquivo YAML pode conter regras de Service e Deployments juntos, ambos serão aplicados separadamentos de acordo com a ordem no arquivo.


```mermaid
graph LR
A[Deploy Command] --> B((Creating))
A --> C(Updating)
B --> D(Has Image)
B --> E(No Image)
C --> D
C --> E
E --> H{Finish Deploy}
D --> F(Has Container)
F --> H
```

#### image

- -c, --container string    Container Name
- -d, --deployment string   Deployment Name
- -f, --filename string     That contains the configuration to apply
- -i, --image string        Name of Image
- -n, --namespace string    Namespace name

Commando para atualizar uma nova imagem.

Deve-se passar como obrigatório "namespace" , "deployment" e "image"

Em caso do deployment não existir será criar um novo  caso um novo arquivo for passado "filename"

Para alterar imagem de um container específico deve-se passar o nome do container em "container"

#### restart

- -d, --deployment string   Deployment Name
- -n, --namespace string    Namespace name


#### configmap

- -d, --deployment string   Deployment Name
- -n, --namespace string    Namespace name
- --name string   Set ConfigMap Name


#### scale

- -d, --deployment string   Deployment Name
- -n, --namespace string    Namespace name
- --replicas int32      Numbers of Replicas

Commando para aumentar ou diminir quantidade de pods.

Deve-se passar como obrigatório "namespace" , "deployment" e "replicas"

#### undo

- -d, --deployment string   Deployment Name
- -n, --namespace string    Namespace name
- --revision int        Revision Numbe

Se não for passado numero revision a última revisão será aplicada.

